<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Categories routes
Route::resource('categories', 'CategoriesController');

Route::get('categories/categories', 'CategoriesController@index');

Route::get('categories/category/{$id}', 'CategoriesController@show');


// products routes
Route::resource('products', 'ProductsController');

Route::get('products/product/{$id}', 'ProductsController@show');

// shoppingcart routes
Route::resource('shoppingcart', 'ShoppingcartController');

Route::get('shoppingcart/shoppingcart', 'ShoppingcartController@index');

